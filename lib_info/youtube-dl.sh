#!/usr/bin/env bash
#
#

setDirsUser

INSTALLATION_TYPE='user'
APP_NAME='youtube-dl'
DESTINATION_DIR="${DIR_OPTIONAL}"/youtube-dl

PKG_FILE="$(getCachePkgs)"/youtube-dl.zip
SCRIPT_FILE="${DIR_BIN}"/youtube-dl

LINK_FILE=None
APP_VERSION='1.0'
PKG_URL='https://github.com/ytdl-org/youtube-dl/archive/refs/heads/master.zip'
ONLINE_SIZE='None'

HASH_TYPE='sha256'
HASH_VALUE=''


function _uninstall_youtube_dl()
{
    echo ERRO
    echo
    return 1
}


function _install_youtube_dl()
{
    local tmp_dir=$(mktemp -d)

    cd $tmp_dir

    unpackArchive $PKG_FILE $tmp_dir
    mv $(ls -d youtube-dl*) youtube-dl
    cd youtube-dl

    python3 setup.py install --user


    rm -rf $tmp_dir
    return 0
}


function main()
{
	if [[ $1 == 'uninstall' ]]; then
        _uninstall_youtube_dl
    elif [[ $1 == 'install' ]]; then
        
        _install_youtube_dl
    elif [[ $1 == 'get' ]]; then
        download $PKG_URL $PKG_FILE || return 1
    else
        printErro 'Parâmetro incorreto.'
        return 1
    fi

    return 0
}


#main $@
