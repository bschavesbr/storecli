#!/usr/bin/env bash
#
#
# https://gitlab.com/bschavesbr/storecli/-/raw/main/setup.sh?inline=false
#


_temp_dir=$(mktemp -d)
_temp_file=$(mktemp)

URL_STORECLI='https://gitlab.com/bschavesbr/storecli/-/archive/main/storecli-main.zip'



function download()
{

	echo -e "Baixando ... $URL_STORECLI"
	if [[ -x $(command -v curl) ]]; then
		curl -s -S -L -o "$_temp_file" "$URL_STORECLI"
	elif [[ -x $(command -v wget) ]]; then
		wget -q "$URL_STORECLI" -O "$_temp_file"
	else
		echo -e "ERRO ... Instale cur|wget"
		return 1
	fi

}



function install(){

	if [[ ! -x $(command -v unzip) ]]; then
		echo -e "ERRO ... Instale o pacote unzip"
		return 1
	fi

	cd "$_temp_dir"
	unzip "$_temp_file" -d "$_temp_dir" 1> /dev/null

	mv $(ls -d storecli*) storecli
	cd storecli
	chmod +x setup.sh
	./setup.sh 

	rm  -rf "$_temp_dir"

}


function main()
{
	download || return 1
	install || return 1
	return 0
}


main "$@" || exit 1

exit 0














